use traits::{
    Draw,
    Update,
    Accelerate,
    Collide,
};
use draw::draw_circle;
use context::DrawContext;

pub struct Ball {
    pub pos: [f32;2],
    pub rad: f32,
    pub col: [f32;4]
}

impl Default for Ball {
    fn default() -> Self {
        Self {
            pos: [0.0,0.0],
            rad: 1.0,
            col: [1.0,1.0,1.0,1.0]
        }
    }
}

impl Draw for Ball {
    fn draw(&self, ctx: &mut DrawContext) {
        draw_circle(self.pos, self.rad, self.col, ctx)
    }
}

impl Collide for Ball {
    fn collide(a: &mut Self, b: &mut Self) {
        let dis_x = (b.pos[0]-a.pos[0]);
        let dis_y = (b.pos[1]-a.pos[1]);
        let dis = dis_x.hypot(dis_y);
        let srad = a.rad+b.rad;
        if 0.0 < dis && dis < srad {
            let fac = (dis-srad)/dis/2.0;
            let acc_x = dis_x*fac;
            let acc_y = dis_y*fac;
            let acc_a = [acc_x, acc_y];
            let acc_b = [-acc_x, -acc_y];
            a.accelerate(acc_a);
            b.accelerate(acc_b);
        }
    }
}

impl Update for Ball {
    fn update(&mut self) {
        
    }
}

impl Accelerate<[f32;2]> for Ball {
    fn accelerate(&mut self, acc: [f32;2]) {
        self.pos[0]+=acc[0];
        self.pos[1]+=acc[1];
    }
}

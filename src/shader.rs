pub mod circle {
    pub static VS_CODE: &'static str = "
        #version 450
        void main() {}
    ";

    pub static GS_CODE: &'static str = "
        #version 450

        layout(points) in;
        layout(triangle_strip, max_vertices = 4) out;
        
        uniform float rad;
        uniform float zoom;

        uniform vec2 pos;
        uniform uvec2 size;
        uniform vec2 view;
        
        out vec2 epos;

        void main() {
            epos = vec2(1,1);
            gl_Position=vec4((pos+epos*rad-view)*zoom*vec2(1/float(size.x), 1/float(size.y)), 0, 1);
            EmitVertex();
            epos = vec2(1,-1);
            gl_Position=vec4((pos+epos*rad-view)*zoom*vec2(1/float(size.x), 1/float(size.y)), 0, 1);
            EmitVertex();
            epos = vec2(-1,1);
            gl_Position=vec4((pos+epos*rad-view)*zoom*vec2(1/float(size.x), 1/float(size.y)), 0, 1);
            EmitVertex();
            epos = -vec2(1,1);
            gl_Position=vec4((pos+epos*rad-view)*zoom*vec2(1/float(size.x), 1/float(size.y)), 0, 1);
            EmitVertex();
        }
    ";

    pub static FS_CODE: &'static str = "
        #version 450

        in vec2 epos;

        uniform vec4 col;
        
        out vec4 color;
        
        void main() {
            if(length(epos)>1.0)
                discard;
            color = col;
        }
    ";
}

pub mod polygon {
    pub static VS_CODE: &'static str = "
        #version 450
        
        in vec2 in_pos;
        
        uniform float zoom;

        uniform float angle;
        uniform vec2 pos;
        uniform uvec2 size;
        uniform vec2 view;
        
        uniform vec2 dir;

        void main() {
            float sin = sin(angle);
            float cos = cos(angle);
            vec2 in_pos_r = pos+in_pos*cos+vec2(in_pos.y,-in_pos.x)*sin-view;
            vec2 pos_r = in_pos_r*dir.x+vec2(in_pos_r.y,-in_pos_r.x)*dir.y;
            gl_Position = vec4((pos_r)*zoom*vec2(1/float(size.x), 1/float(size.y)), 0, 1);
        }
    ";

    pub static FS_CODE: &'static str = "
        #version 450

        uniform vec4 col;

        uniform float zoom;

        uniform float angle;
        uniform uvec2 size;
        uniform vec2 view;
        uniform vec2 dir;
        
        out vec4 color;
        void main() {
            color = col;
        }
    ";
}

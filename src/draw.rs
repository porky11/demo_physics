use glium::{self, Surface};
use context::DrawContext;



pub fn draw_circle(pos: [f32;2], rad: f32, color: [f32;4], ctx: &mut DrawContext) {
    let uniforms = uniform! {
        pos: pos,
        rad: rad,
        col: color,
        rot: ctx.view.dir.sin_cos(),
        size: ctx.view.size,
        view: ctx.view.pos,
        zoom: ctx.view.zoom,
    };
    
    let params = glium::DrawParameters {
        blend: glium::Blend::alpha_blending(),
        .. Default::default()
    };

    ctx.target.draw(glium::vertex::EmptyVertexAttributes{len: 1},
                    glium::index::NoIndices(glium::index::PrimitiveType::Points),
                    &ctx.circle_program, &uniforms, &params).unwrap();
}


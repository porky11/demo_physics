use glium::{
    Frame,
    Program,
    Display
};

pub struct View {
    pub pos: [f32;2],
    pub zoom: f32,
    pub dir: f32,
    pub size: [u32;2],
}

pub struct DrawContext<'a, 'b, 'c, 'd> {
    pub target: &'a mut Frame,
    pub display: &'b Display,
    pub polygon_program: &'c Program,
    pub circle_program: &'c Program,
    pub view: &'d View
}



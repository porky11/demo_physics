use context::DrawContext;

use traits::{
    Draw,
    Collide,
    Update
};

pub struct World<T>(pub Vec<T>);

impl<T> World<T> where T: Collide {
    pub fn act(&mut self) {
        for i in 0..self.0.len() {
            if let Some((a, rest)) = self.0[i..].split_first_mut() {
                for b in rest {
                    Collide::collide(a,b);
                }
            }
        }
    }
}

impl<T> Draw for World<T> where T: Draw {
    fn draw(&self, ctx: &mut DrawContext) {
        for object in &self.0 {
            object.draw(ctx)
        }
    }
}

impl<T> Update for World<T> where T: Update {
    fn update(&mut self) {
        for object in &mut self.0 {
            object.update()
        }
    }
}



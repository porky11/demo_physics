#[macro_use]
extern crate glium;

mod shader;
mod context;
mod traits;
mod draw;
mod world;
mod ball;

fn main() {

    use glium::Surface;

    let mut left = false;
    let mut right = false;
    let mut up = false;
    let mut down = false;
    
    let mut view = context::View {
        zoom: 64.0,
        size: [1920, 1080],
        dir: 0.0,
        pos: [0.0, 0.0]
    };


    let mut events_loop = glium::glutin::EventsLoop::new();
    
    let window = glium::glutin::WindowBuilder::new()
        .with_dimensions(view.size[0], view.size[1]);
    
    let context = glium::glutin::ContextBuilder::new();
    
    let display = glium::Display::new(window, context, &events_loop).unwrap();
    
    let circle_program = glium::Program::from_source(&display,
        shader::circle::VS_CODE,
        shader::circle::FS_CODE,
        Some(shader::circle::GS_CODE)).unwrap();

    let polygon_program = glium::Program::from_source(&display,
        shader::polygon::VS_CODE,
        shader::polygon::FS_CODE,
        None).unwrap();

    let mut closed = false;
    
    let mut world = world::World(
        vec![
            ball::Ball {
                ..Default::default()
            },
            ball::Ball {
                pos: [0.0,4.0],
                col: [1.0,0.0,0.0,1.0],
                ..Default::default()
            },
            ball::Ball {
                pos: [0.0,-4.0],
                col: [1.0,1.0,0.0,1.0],
                ..Default::default()
            },
            ball::Ball {
                pos: [-4.0,0.0],
                col: [0.0,1.0,0.0,1.0],
                ..Default::default()
            },
            ball::Ball {
                pos: [4.0,0.0],
                col: [0.0,0.0,1.0,1.0],
                ..Default::default()
            }
        ]
    );
    
    while !closed {
        use std::time;

        let now = time::Instant::now();
        
        use traits::*;
        
        fn num(x: bool) -> f32 {
            if x {1.0} else {0.0}
        }
        
        let vec = [
            (num(right)-num(left))*0.1,
            (num(up)-num(down))*0.1,
        ];
        
        world.0[0].accelerate(vec);
        
        world.act();
        
        world.update();

        let mut target = display.draw();
        target.clear_color(0.0,0.0,0.0,1.0);
        {
            let mut ctx = context::DrawContext {
                target: &mut target,
                display: &display,
                circle_program: &circle_program,
                polygon_program: &polygon_program,
                view: &view
            };
            world.draw(&mut ctx)
        }
        target.finish().unwrap();

        events_loop.poll_events(|event| {
            if let glium::glutin::Event::WindowEvent {event, ..} = event {
                use glium::glutin::WindowEvent::*;
                match event {
                    Closed => closed = true,
                    Resized(x,y) => view.size=[x,y],
                    KeyboardInput{input: glium::glutin::KeyboardInput{
                        virtual_keycode: Some(code), state: key_state, ..},
                    ..} => {
                        use glium::glutin::ElementState::*;
                        let pressed = match key_state {
                            Pressed => true,
                            Released => false
                        };
                        use glium::glutin::VirtualKeyCode::*;
                        match code {
                            Escape => closed = true,
                            Left => left = pressed,
                            Right => right = pressed,
                            Up => up = pressed,
                            Down => down = pressed,
                            _ => ()
                        }
                    },
                    _ => ()
                }
            }
        });
        {
            use std::thread;
            use std::time::Duration;
            
            let fps = 30;

            let wait = Duration::from_millis(1000/fps);

            let elapsed = now.elapsed();

            if wait>elapsed {
                thread::sleep(wait-elapsed);
            } else {
                println!{"Low performance!"};
            }
        }
    }
}


use context::DrawContext;

pub trait Draw {
    fn draw(&self, &mut DrawContext);
}

pub trait Collide {
    fn collide(&mut Self, &mut Self);
}

pub trait Update {
    fn update(&mut self);
}

pub trait Accelerate<T> {
    fn accelerate(&mut self, T);
}

